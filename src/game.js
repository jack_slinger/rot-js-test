import Player from "./player";

var Game = {
  display: null,

  init: function () {
    this.display = new ROT.Display();
    document.body.appendChild(this.display.getContainer());
    this.display.setOptions({ fontSize: 32 });
    this._generateMap();

    var scheduler = new ROT.Scheduler.Simple();
    scheduler.add(this.player, true);
    this.engine = new ROT.Engine(scheduler);
    this.engine.start();
  },
};

Game.map = {};
Game.player = null;
Game.engine = null;

Game._generateMap = function () {
  var digger = new ROT.Map.Digger();
  var freeCells = [];

  var digCallback = function (x, y, value) {
    if (value) {
      return;
    } /* do not store walls */

    var key = x + "," + y;
    freeCells.push(key);
    this.map[key] = ".";
  };
  digger.create(digCallback.bind(this));

  this._generateBoxes(freeCells);

  this._drawWholeMap();
  this._createPlayer(freeCells);
};

Game._generateBoxes = function (freeCells) {
  for (var i = 0; i < 10; i++) {
    var index = Math.floor(ROT.RNG.getUniform() * freeCells.length);
    var key = freeCells.splice(index, 1)[0];
    this.map[key] = "*";
  }
};

Game._drawWholeMap = function () {
  for (var key in this.map) {
    var parts = key.split(",");
    var x = parseInt(parts[0]);
    var y = parseInt(parts[1]);
    this.display.draw(x, y, this.map[key]);
  }
};

Game._createPlayer = function (freeCells) {
  var index = Math.floor(ROT.RNG.getUniform() * freeCells.length);
  var key = freeCells.splice(index, 1)[0];
  var parts = key.split(",");
  var x = parseInt(parts[0]);
  var y = parseInt(parts[1]);
  this.player = new Player(x, y, this);
};

export default Game;
