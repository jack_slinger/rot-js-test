var Player = function (x, y, game) {
  this._x = x;
  this._y = y;
  this._game = game;
  this._draw();
};

Player.prototype._draw = function () {
  this._game.display.draw(this._x, this._y, "@", "#ff0");
};

Player.prototype.act = function () {
  this._game.engine.lock();
  /* wait for user input; do stuff when user hits a key */
  window.addEventListener("keydown", this);
};

Player.prototype.handleEvent = function (e) {
  // Numpad keys
  var keyMap = {};
  keyMap[38] = 0;
  keyMap[33] = 1;
  keyMap[39] = 2;
  keyMap[34] = 3;
  keyMap[40] = 4;
  keyMap[35] = 5;
  keyMap[37] = 6;
  keyMap[36] = 7;

  var code = e.keyCode;

  if (!(code in keyMap)) {
    return;
  }

  var diff = ROT.DIRS[8][keyMap[code]];
  var newX = this._x + diff[0];
  var newY = this._y + diff[1];

  var newKey = newX + "," + newY;
  if (!(newKey in this._game.map)) {
    return;
  } /* cannot move in this direction */

  this._game.display.draw(
    this._x,
    this._y,
    this._game.map[this._x + "," + this._y],
  );
  this._x = newX;
  this._y = newY;
  this._draw();
  window.removeEventListener("keydown", this);
  this._game.engine.unlock();
};

export default Player;
